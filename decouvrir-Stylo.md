---
linkcolor: blue
papersize: a4
documentclass: scrreprt
---

<style>
  div.info {  background-color: aliceblue;  border: solid 1px darkblue;  padding: 0 1em;  margin-bottom: 1em;  }
  div.warning {  background-color: bisque;  border: solid 1px darkorange;  padding: 0 1em;  margin-bottom: 1em;  }
  div.consigne {  background-color: #dff0d8;  border: solid 1px seagreen;  padding: 0 1em;  margin-bottom: 1em;  }
  div.consigne::before {content: "Consignes"; position: relative; top:-15px; left: -10px; background-color: #dff0d8; padding: 0 4px; color: seagreen; font-variant: all-small-caps;}
  div.sourceCode { border: 1px gray solid; padding: 0.5em; background-color: cornsilk; line-height: 1.2em;}
  code {background-color: cornsilk; font-size: small; }
  div.center {text-align: center;}
  body {  counter-reset: countFigure;}
  figcaption { counter-increment: countFigure;}
  figcaption::before { content: "Figure " counter(countFigure, roman) " : ";}
  a { font-weight: 600;}
</style>

:::info
Une version PDF de ce document est disponible [sur ce lien](https://cml.arawa.fr/s/PeNqXbcHfKXMESQ).
:::

## 1. Prise en main du format Markdown

Le format Markdown est un format nativement web, c'est-à-dire qu'il est directement transposable au format HTML. Il permet d'écrire dans le web et pour le web, c'est-à-dire : dans notre écosystème de savoir.

Utiliser le format Markdown, c'est :

1. **choisir la transparence** → format _plain text_ 
2. **adopter la simplicité et donc la rigueur** → on en maîtrise très vite a syntaxe, et donc on l'applique.
3. **séparer le fond et de la forme** → on ne s'occupe que de structure, c'est-à-dire que de l'aspect scientifique de votre contenu : le texte, et sa structuration dans l'écosystème du savoir. Markdown → HTML + CSS
4. **progresser dans sa littératie numérique** → car utilisé sur de multiples plateformes web
5. **garantir la pérennité** → le _plain text_ était utilisé au tout début des ordinateurs, il restera lisible dans le temps long par toutes les machines et intelligible par tous les humains.


:::consigne
**→ 10 min pour faire ses premiers pas en Markdown : [tutoriel](https://www.markdowntutorial.com/fr/)**
:::

:::warning

Stylo utilise le convertisseur de document Pandoc ([pandoc.org](https://pandoc.org/)) qui propose une syntaxe étendue du Markdown permettant de produire tous les éléments éditoriaux de la publication scientifique classique.

→ Voir [la documentation complète du Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown)

:::

## 2. Prise en main de Stylo

:::warning

Vous êtes invité à vous créer un accès à Stylo en vous rendant sur [stylo.huma-num.fr](https://stylo.huma-num.fr/)

- soit avec un compte HumanID  → connect (si vous avez un HumanID) ou  [create a Huma-Num account](https://humanid.huma-num.fr/register?service=https://stylo.huma-num.fr/authorization-code/callback)  
- soit avec un compte stylo local → [create an account](https://stylo.huma-num.fr/register)

:::


### 2.1. L'interface d'édition

:::consigne
**→ Ouvrez le document _How to Stylo_**
:::
 
 Ce document est un tutoriel et un bac-à-sable : vous pouvez l'éditer et prévisualiser les changements en cliquant sur [Preview]. 

À découvrir :

1. les modules
2. les différentes sections du document How-to-Stylo
3. la prévisualisation HTML et l'annotation 

### -- Prévisualiser son document

La prévisualisation est une version HTML du document, combinant les trois sources d'un document Stylo: le corps de texte, la bibliographie, et les métadonnées.

Fonctionnalités: 

- encart des métadonnées
- sommaire cliquable
- références cliquables
- outil d'annotation en ligne (Hypothes.is)

:::consigne

1. Cliquez sur le bouton Preview. Un nouvel onglet s'est ouvert. Explorez le document (appel de référence, bibliographie, notes de bas de page, etc.). Comparez le résultat à la source.
2. Créez une annotation en surlignant un mot ou une phrase. Si nécessaire, connectez-vous à Hypothes.is ou créez-vous un compte.
:::

### -- Versionner son document

Votre document affiche soit "la version en cours d'édition", soit une version antérieure.

Les principes essentiels : 

- toutes vos modifications sont enregistrées en temps réel. 
- les "versions" sont des états du texte "déclarés", que l'on souhaite stabiliser (et documenter)
- une version comprend les trois sources : corps de texte, métadonnées, bibliographie
- on peut remonter dans le temps, mais les versions antérieures ne sont pas modifiables
- on peut comparer deux versions entre elles, y compris la version "en cours"
- bonnes pratiques: documenter pour soi et pour les autres

:::consigne

**→ Explorez le module de versionning**

1. faites des modifications dans le corps du texte et dans les métadonnées
2. créez des versions au fur-et-à mesure en les documentant précisément
3. naviguez entre les versions
4. comparez les versions
5. retournez à l'édition et comparez avec une version antérieure
:::

### -- Gérer les métadonnées

1. Métadonnées auteur (_basic mode_) : titre, auteur·e·s, résumé·s, mot-clés, langage, license
2. Métadonnées éditeur (_editor mode_) : id, url, mot-clés référentiels, catégories, diffusion, direction, production, dossier, direction dossier, révision/évaluation, traduction, transcription
3. Métadonnées expert (_raw mode_) : paramétrage d'export, ajout de métadonnées

:::consigne
- ajoutez un auteur en utilisant la recherche Isidore
- ajoutez des métadonnées, et prévisualisez le résultat
- créez une nouvelle version et documentez vos modifications
:::

### 2.2. La gestion des documents

:::consigne
**→ Revenez à la liste des Articles en cliquant sur [Articles]**
:::

À découvrir :

1. Fonctionnalités du document
2. Gestion de tags
3. La documentation Stylo
4. Les paramètres du compte
5. Création de documents

:::consigne
**→ Créez un document en préparation de l'exercice suivant**

1. assignez lui un ou plusieurs tags
2. ouvrez le document en édition
:::


## 3. Éditer un texte dans Stylo 

:::consigne
1. Accédez [à ces ressources](https://cml.arawa.fr/s/YBZ6sLcMN6njXjH), puis cliquez sur le fichier `SP1359.txt`
2. Sélectionner le texte (CTRL+A), copiez-le (CTRL+C) et collez-le dans le document Stylo ouvert (CTRL+V)
:::

### -- Structurer le texte 

:::consigne
**Éditez le texte en vous référant au texte publié dans la revue Sens Public : [sens-public.org/articles/1359/](http://sens-public.org/articles/1359/)**

- Que fait-on des métadonnées (titre, auteur, date, mots-clés, résumés, etc.) ?
- Balisez la citation (dernier paragraphe)
- Ajoutez des titres intermédiaires
- Ajoutez des notes de bas de page

:::

### -- Ajouter des références

:::{.warning .center}
**Bibliographie = Données**
:::

Les références bibliographiques sont structurées dans un format bibtex. Exemple :

```bibtex
@article{merzeau_nouvelle_2007,
	title = {Une nouvelle feuille de route},
	issn = {1771-3757},
	url = {https://www.cairn.info/revue-medium-2007-4-page-3.htm},
	doi = {10.3917/mediu.013.0003},
	language = {fr},
	number = {13},
	urldate = {2018-05-10},
	journal = {Médium},
	author = {Merzeau, Louise},
	year = {2007},
	pages = {3--15},
}
```

La « clé bibtex » `@merzeau_nouvelle_2007` nous permet de déclarer une citation dans le corps du texte.
Exemple en Markdown :

```markdown
Dans cet article qui apparaît comme un jalon important de sa 
« feuille de route » [@merzeau_nouvelle_2007], Louise 
Merzeau identifie des pistes de réflexion valides [...]
```

Cela donnera dans le texte final : 

> Dans cet article qui apparaît comme un jalon important de sa 
> « feuille de route » (Merzeau 2007), Louise 
> Merzeau identifie des pistes de réflexion valides [...]

Regardons le résultat en HTML :

```html
<span class="citation" data-cites="merzeau_feuille_2007">
   (Merzeau <a href="#ref-merzeau_feuille_2007" role="doc-biblioref">2007</a>)
</span>
```

:::info
**Syntaxe Markdown**

|Markdown|Chicago (auteur date)|
|:--|:--|
| `[@shirky_here_2008]`       | (Shirky 2008)       |
| `[@shirky_here_2008, p194]` | (Shirky 2008, p194) |
| `@shirky_here_2008`         | Shirky (2008)       |
| `[-@shirky_here_2008]`      | (2008)              |

**Ici le stylo Chicago (fr) est utilisé, mais nous pouvons utiliser [n'importe quel style bibliographique](https://www.zotero.org/styles), y compris un style "note de bas de page".** (Nous verrons ça dans les exports)

→ Pour aller plus loin, se référer :  

1. [à la documentation Stylo](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/bibliographie.md#Ins%C3%A9rer_une_r%C3%A9f%C3%A9rence_bibliographique)
2. [à la documentation de Pandoc](https://pandoc.org/MANUAL.html#citation-syntax)
3. [le répertoire de styles bibliographiques (maintenu par Zotero)](https://www.zotero.org/styles)
:::

Sur Stylo, 2 possibilités pour gérer ses références bibliographiques :

1. À partir d'une collection Zotero
2. À partir de références au format bibtex


:::consigne

**Consignes :**

1. Importez les références de l'article en utilisant les différentes méthodes (bibtex, zotero)
    1. [→ téléchargez le fichier SP1359.bib](https://cml.arawa.fr/s/YBZ6sLcMN6njXjH), ou testez votre propre bibtex
    2. [→ accédez à la collection Zotero](https://www.zotero.org/groups/4590953/atelierfplab/collections/PNEK67GI), ou testez avec votre propre compte et collections Zotero.
2. Intégrez les références dans le texte selon la syntaxe Markdown (voir ci-dessus)
3. Ajoutez un titre de niveau 2 en fin d'article "Bibliographie" ou "Références"

:::


### -- Ajouter des images

Stylo n'intègre pas de gestionnaire de médias ou d'images. Il faut donc héberger les images ailleurs sur le web : serveur personnel, services ([imgur](https://imgur.com/)), CMS (Wordpress), forges (GitLab, GitHub), entrepôts ([Nakala](https://nakala.fr), [Zenodo](https://zenodo.org), [MediHAL](https://medihal.archives-ouvertes.fr/)). 

:::warning
Pour cet export PDF, certaines images ont été supprimées du document pour que l'export PDF puisse fonctionner.
:::

:::info
**Syntaxe Markdown: image depuis le web**

La syntaxe pour une image se présente ainsi : `![légende](url-image)`

Par exemple : `![Légende de l'image](media/URFIST__Decouvrir_Stylo_partage-latest-img2.png)` donnera :

![Légende de l'image](media/URFIST__Decouvrir_Stylo_partage-latest-img2.png)

Cet objet image en Markdown a été interprété comme un élément html `<figure>`, et la Légende comme un élément html `<figcaption>`.

Selon les formats d'export envisagés, il est possible de paramétrer certains éléments de l'image, notamment sa taille.

```html
![Légende de mon image](https://monsite.org/img/monimage.jpg){ width=300px }

![Légende de mon image](https://monsite.org/img/monimage.jpg){ width=80% }
```

Précisions (unités, usages) sur [la documentation Pandoc](https://pandoc.org/MANUAL.html#images).


:::

Pour un article scientifique, une bonne pratique peut être d'utiliser un entrepôt institutionnel, comme NAKALA, proposant différentes possibilités d'éditorialisation de l'image : raw, embed, iiif. Pour les exemples suivants, nous utilisons le fichier `9dffa7e40c1d2ebaeb882f7ad92065d7000ec139` de la donnée NAKALA suivante [DOI:10.34847/nkl.efa4zw2z/](https://doi.org/10.34847/nkl.efa4zw2z).

:::info

**Syntaxe Markdown: image depuis NAKALA (raw)**

`![Une image venant de NAKALA (raw)](https://api.nakala.fr/data/10.34847/nkl.efa4zw2z/9dffa7e40c1d2ebaeb882f7ad92065d7000ec139)`

![Une image venant de NAKALA (raw)](https://api.nakala.fr/data/10.34847/nkl.efa4zw2z/9dffa7e40c1d2ebaeb882f7ad92065d7000ec139)

Cet objet image en Markdown a été interprété comme un élément html `<figure>`, et la Légende comme un élément html `<figcaption>`.

:::

NAKALA propose également une visionneuse (embed) qui peut se révéler pertinente **pour une publication web**.

:::info

**Syntaxe Markdown: image depuis NAKALA (embed)**

`<iframe src="https://api.nakala.fr/embed/10.34847/nkl.efa4zw2z/9dffa7e40c1d2ebaeb882f7ad92065d7000ec139" width="80%" height="300px"></iframe>`

<iframe src="https://api.nakala.fr/embed/10.34847/nkl.efa4zw2z/9dffa7e40c1d2ebaeb882f7ad92065d7000ec139" width="100%" height="300px"></iframe>


On utilise ici un élément html `<iframe>`.

:::


:::info

**Syntaxe Markdown: image depuis NAKALA (IIIF)**

`![Une image venant de NAKALA (IIIF) ([doi:10.34847/nkl.efa4zw2z](https://doi.org/10.34847/nkl.efa4zw2z))](media/URFIST__Decouvrir_Stylo_partage-latest-img1.jpg)`

![Une image venant de NAKALA (IIIF)  ([doi:10.34847/nkl.efa4zw2z](https://doi.org/10.34847/nkl.efa4zw2z))](media/URFIST__Decouvrir_Stylo_partage-latest-img1.jpg)

Cet objet image en Markdown a été interprété comme un élément html `<figure>`, et la Légende comme un élément html `<figcaption>`. On exploite le protocole IIIF pour opérer quelques manipulations d'images (région, taille, rotation, qualité d'image). 

Pour aller plus loin, se référer :

1. à [la documentation de NAKALA](https://documentation.huma-num.fr/nakala/) et notamment [Présenter des données NAKALA sur un site](https://anf2021-humanum.sciencesconf.org/data/pages/ANF_HN_2021_Module_6.pdf)
2. à [la documentation de IIIF](https://iiif.io/api/image/3.0/#4-image-requests)

:::

:::warning
Attention, parmi les 3 méthodes d'intégration d'image depuis NAKALA, seule la méthode IIIF permettra un export PDF.
:::

:::consigne
**Ajoutez une ou plusieurs images dans le texte.**
:::

### -- Typographie

- **Espaces insécables** : les espaces insécable sont représentés dans l'éditeur par un point médian léger `«·Bonjour·»`
    - → `CTRL+SHIFT+SPACE` 
- **Contenu multilingue** : les éléments balisés par une information de langue seront interprétés en tant que tel.
     - exemple: `Il peut être utile ["to write in English"]{lang=en}.` → Il peut être utile ["to write in English"]{lang=en}.
     - Pour aller plus loin, voir [la documentation Pandoc](https://pandoc.org/MANUAL.html#language-variables)
- **Balisage sémantique** : `Voici un élément [qualifié]{.adjectif}`
     - Stylo intègre quelques marqueurs éditoriaux classiques : épigraphe, dédicace, etc. Mais aussi des marqueurs scientifiques : question, thèse, etc.
     - Voir [la liste complète sur la documentation Stylo](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/syntaxemarkdown.md#Balisage_s%C3%A9mantique)
- **espace, paragraphe et saut à la ligne** : en syntaxe Markdown, le saut à la ligne ne correspond pas (normalement) à un changement de paragraphe. En principe, un saut à la ligne sera interprété comme un simple espace. De même un double espace ne sera interprété que comme un simple espace. Changer de paragraphe impose de sauter une ligne.
     - Voir [la leçon Paragraphes](https://www.markdowntutorial.com/fr/lesson/7/) du tutoriel Markdown.


## 4. Exporter le document

L'export de document est une conversion des sources en un format de travail ou de diffusion. La conversion consiste à combiner les métadonnées (.yaml), les références bibliographiques (.bib) et le corps de texte (.md) en un seul document.

**Options :**

- formats de sortie: 
   - **HTML5**: un fichier HTML exhaustif comprenant toutes les métadonnées (balises `<meta>`)
   - **ZIP**: un fichier zip contenant toutes les sources (markdown, yaml, bibtex, images)
   - **PDF**: un fichier PDF produit avec LaTeX
   - **LaTeX**: un fichier LaTeX et les images sources pour personnaliser une sortie PDF
   - **XML** (Erudit): un fichier XML selon le schéma XML Erudit Journal 
   - **ODT**: un fichier .odt
   - **DOCX**: un fichier .docx
   - **EPUB**: un fichier EPUB exploitable
   - **TEI**: export TEI basique actuellement.
   - **ICML**: format d'import pour InDesign. Au premier import, InDesign propose de faire un _mapping_ entre les champs XML (ICML) et les éléments éditoriaux.
- style de citation (footnote / inline):
   - Chicago (modified): format `(auteur date)`
   - Chicago Full Note: format "notes de bas de page"
   - Lettres et sciences humaines: format créé par la bibliothèque de l'Université de Montréal et largement utilisé. 
- avec ou sans table des matières

**Page d'export :** 

- transparence des traitements (reproductibilité)
- option supplémentaire pour choisir son style de citation (voir [le répertoire de CSL maintenu par Zotero]( https://www.zotero.org/styles))

:::consigne
**→ Explorez les options et les formats d'export.**

1. choisissez le format HTML, ouvrez le fichier html exporté, téléchargez le fichier zip produit et explorez son contenu
2. choisissez le format PDF, ouvrez le fichier PDF exporté, téléchargez le fichier zip produit et explorez son contenu

:::

:::warning
Un nouveau module d'export est en cours de développement. Il implémentera les exports vers Lodel et Métopes, et permettra de paramétrer plus finement les exports. En attendant, on doit paramétrer certains aspects "à la main" (voir ci-dessous).
:::

### -- Convertir un docx en markdown

Deux méthodes : 

1. vous pouvez utiliser le service en ligne [word2md.com](https://word2md.com/) et copier-coller le résultat au format Markdown dans votre document Stylo.
2. si vous êtes à l'aise avec la ligne de commande, vous pouvez installer [Pandoc](https://pandoc.org) et suivre ces différents tutoriels Pandoc: [[1]](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown), [[2]](https://www.tutorialsteacher.com/articles/convert-word-doc-to-markdown#pandoc), [[3]](https://blog.atwork.at/post/Convert-documents-with-Pandoc).

### -- Personnaliser les exports

Stylo utilise le moteur de conversion Pandoc qui offre plusieurs possibilités de personnalisation des exports.

1. on peut agir sur certains paramètres en ajoutant des options et des variables au document
2. on peut agir sur les _"templates"_ de certains formats pour modifier la présentation finale → nécessite d'utiliser Pandoc directement en ligne de commande 
3. on peut scripter pour réaliser des chaînes de traitements sur mesure → nécessite de maîtriser un langage de programmation (Bash, Python, etc.)

Agir sur certains paramètres ou variables nécessite de bien lire [la documentation Pandoc](https://pandoc.org/MANUAL.html#options) ! Ces paramètres peuvent être passés directement dans le yaml de votre document.

Exemples d'options paramétrables : avoir une table des matières, définir la profondeur de la table des matières, définir le titre de la table des matières, choisir d'avoir appels de citation liés à la bibliographie, choisir la couleur des liens, définir l'encodage des caractères (ASCII), choisir le type de section (HTML), définir la police de caractère, définir le format de la feuille (PDF, docx, odt), type de document (LaTeX, PDF), 

:::info

Pour passer des options ou variables, vous pouvez ajouter un en-tête yaml en début de document:

```yaml
  ---
papersize: a4
linkcolor: red
toc: true
---

le début de mon document
```
:::

:::info
**Notez :**

Stylo utilise des _templates_ sur mesure. Certaines variables usuelles ne sont pas pris en compte.

Certains formats d'export sont peu paramétrables (docx, odt, XML TEI), à moins de travailler les templates.

Certaines options ne sont pas applicables dans le cas de Stylo dont la conversion se fait sur le serveur Stylo.
:::

:::consigne
**→ Explorez la documentation Pandoc pour paramétrer vos exports :

- [options générales](https://pandoc.org/MANUAL.html#options)
- [options d'exports générales](https://pandoc.org/MANUAL.html#general-writer-options) et [options spécifiques à certains exports](https://pandoc.org/MANUAL.html#options-affecting-specific-writers)
- [variables pour HTML](https://pandoc.org/MANUAL.html#variables-for-html) et [variables pour PDF (LaTeX)](https://pandoc.org/MANUAL.html#variables-for-latex)


1. Choisissez un format de sortie (PDF, HTML) selon vos pratiques éditoriales
2. Ajoutez un en-tête yaml à votre document et testez différentes options/variables :
    1. PDF, HTML: Ajoutez une table des matières et modifiez sa profondeur
    2. PDF, HTML: Numérotez les sections
    3. PDF, HTML : Modifiez la couleur des liens (voir [la documentation W3School sur les couleurs](https://www.w3schools.com/colors/colors_names.asp))
    4. PDF: Modifiez le format de page et les marges 

:::


### -- Personnaliser la CSS

Pour les exports HTML et EPUB, on peut aussi jouer sur le style CSS en ajoutant une balise `<style>` en début de document :

```html
<style>
  body { width: 30em; margin: 0 auto; }
  p { color: red; }
  h2 { margin-top: 2em; } 
</style>

```
ou dans un en-tête yaml:

```yaml
header-includes: |
  <style>
    body { width: 30em; margin: 0 auto; }
    p { color: red; }
    h2 { margin-top: 2em; } 
  </style>

```

:::info
**→ Hack: Numéroter les figures dans la prévisualisation HTML ou l'export HTML :**

```html
<style>
  body {counter-reset: countFigure;}
  figcaption {counter-increment: countFigure;}
  figcaption::before {content: "Figure " counter(countFigure, roman) " : ";}
</style>
```
voir [le snippet dédié](https://gitlab.huma-num.fr/-/snippets/25)
:::



### -- Chaînes de traitements pour les revues 

[Plusieurs chaînes de traitement dédiées](https://stylo-export.ecrituresnumeriques.ca/) ont été réalisées à la demande de revues.

:::info
**yaml par défaut**

Stylo permet de configurer les métadonnées présentes par défaut dans les futurs documents Stylo. Il s'agit de modifier le « yaml par défaut » dans les paramètres de compte. Vous pouvez par exemple ajouter vos données d'auteur, la licence sous laquelle vous publiez habituellement, etc. Pour les revues, ce peut être utile pour ne pas répéter les mêmes métadonnées sur un lot d'articles. 

:::consigne
→ Dans la barre de navigation, cliquez sur votre username puis copier-coller un yaml personnalisé dans le champ consacré "Default YAML".
:::

:::warning
Le yaml doit être complet (toutes les clés doivent être présentes, même si leur valeur peut être vide). Il est recommandé de partir d'un yaml source, par exemple un yaml exporté ou copié-collé (mode raw) depuis un article stylo. 

Personnaliser le yaml par défaut ne modifie pas les données déjà créées dans Stylo. **Vos anciens articles ne seront pas affectés.** 

Il est important de bien respecter la syntaxe yaml pour modifier le yaml par défaut, au risque de créer des erreurs dans les nouveaux articles. Faites des tests de preview et d'export avant de valider l'utilisation d'un yaml personnalisé


Il est toujours possible de supprimer votre yaml par défaut personnalisé pour revenir à la normal (yaml vierge).
:::
:::

## 5. Personnaliser sa pratique 

1. Hacker Stylo
2. Personnaliser le yaml par défault
2. Explorer la documentation Stylo
3. Explorer la documentation Pandoc (ajout d'options/variables yaml)
4. (Faire) ajouter un export personnalisé 
5. S'émanciper de Stylo → utiliser Pandoc en ligne de commande et élaborer ses propres templates ([voir le tutoriel](https://gitlab.huma-num.fr/ecrinum/manuels/tutoriel-markdown-pandoc/-/tree/master/))
6. Produire une mini-chaîne de traitement → mémoire, thèse, rapport, ouvrage, site web

